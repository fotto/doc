# -*- coding: utf-8 -*-

from docutils.transforms import Transform
from docutils import nodes

# Change the "program" role to uppercase
#
class UppercaseProgram(Transform):
  default_priority = 10

  def apply(self):
    for node in self.document.traverse(nodes.inline):
      if ('program' in node['classes']):
        for child in node.traverse(nodes.Text):
          node.replace(child, nodes.Text(child.astext().upper()))

# Setup
#
def setup(app):
  app.add_transform(UppercaseProgram)
